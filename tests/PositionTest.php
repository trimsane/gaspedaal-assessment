<?php
/**
 * Created by PhpStorm.
 * User: trimsane
 * Date: 7/16/2018
 * Time: 10:45 PM
 */

namespace tests;

use CarGame\Position;
use PHPUnit\Framework\TestCase;
use Utils\Enum;

class PositionTest extends TestCase
{

    public function test__toString()
    {
        $instance = new Position(10, 10, 'N');
        $this->assertEquals('10 10 N', (string)$instance);
    }

    public function dataProvider() {
        return [
            [0, 0, 'B'],
            [1, 10, 'W'],
            [-10, 10, 'N'],
            [10, 10, 'E'],

            [0, 0, 'asd', true],
            [0, 0, 'aa', true]
        ];
    }

    /**
     * @dataProvider dataProvider
     * @param int $x
     * @param int $y
     * @param string $direction
     * @param bool $expectException
     */
    public function test__construct(int $x, int $y, string $direction, bool $expectException = false)
    {
        if ($expectException) {
            $this->expectException(\InvalidArgumentException::class);
        }

        $pos = new Position($x, $y, $direction);

        $this->assertInstanceOf(Position::class, $pos);
    }
}
