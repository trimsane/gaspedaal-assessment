<?php
/**
 * Created by PhpStorm.
 * User: trimsane
 * Date: 7/17/2018
 * Time: 1:44 AM
 */

namespace tests;

use CarGame\Grid;
use PHPUnit\Framework\TestCase;

class GridTest extends TestCase
{
    public function constructorDataProvider() {
        return [
            [20, 20, false],
            [0, 0, false],
            [-1, 20, true],
            [1, -1, true],
        ];
    }

    /**
     * @dataProvider constructorDataProvider
     * @param int $x
     * @param int $y
     * @param bool $expectError
     */
    public function test__construct(int $x, int $y, bool $expectError)
    {
        if ($expectError) {
            $this->expectException(\InvalidArgumentException::class);
        }

        $grid = new Grid($x, $y);

        $this->assertInstanceOf(Grid::class, $grid);
    }

    public static function createGrid() {
        return new Grid(10, 20);
    }

    public function testGetSizeY()
    {
        $actual = self::createGrid();
        $this->assertEquals(20, $actual->getSizeY());
    }

    public function testGetSizeX()
    {
        $actual = self::createGrid();
        $this->assertEquals(10, $actual->getSizeX());
    }
}
