<?php
/**
 * Created by PhpStorm.
 * User: trimsane
 * Date: 7/16/2018
 * Time: 10:46 PM
 */

namespace tests;

use CarGame\MoveCommandEnum;
use PHPUnit\Framework\TestCase;

class MoveCommandEnumTest extends TestCase
{
    public function testConstantsCount()
    {
        $this->assertEquals(3, MoveCommandEnum::count());
    }

    public function testConstants()
    {
        $this->assertEquals(MoveCommandEnum::F()->getValue(), 0);
        $this->assertEquals(MoveCommandEnum::L()->getValue(), -1);
        $this->assertEquals(MoveCommandEnum::R()->getValue(), 1);
    }
}
