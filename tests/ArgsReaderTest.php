<?php
/**
 * Created by PhpStorm.
 * User: trimsane
 * Date: 7/17/2018
 * Time: 5:17 AM
 */

namespace src\Utils;

use Utils\ArgsReader;
use PHPUnit\Framework\TestCase;

class ArgsReaderTest extends TestCase
{
    public function readNextProvider()
    {
        return [
            [
                ["a", "abc"],
                [0, "cats"],
                [false, "beer", 0]
            ]
        ];
    }

    /**
     * @dataProvider readNextProvider
     * @param array $expected
     */
    public function testReadNext(array $expected)
    {
        $reader = new ArgsReader($expected);

        $actual = [];
        while ($value = $reader->readNext()) {
            $actual[] = $value;
        }

        $this->assertEquals($expected, $actual);
    }

    public function moveProvider()
    {
        return [
            [
                [1, 2, 3, 4, 5], 5, 3
            ],
            [
                [1, 2], 2, 1
            ],
            [
                [10, 10, 10, 1, 2], 5, 0
            ]
        ];
    }

    /**
     * @dataProvider moveProvider
     *
     * @param array $args
     * @param int $read
     * @param int $move
     */
    public function testMoveBackward(array $args, int $read, int $move)
    {
        $reader = new ArgsReader($args);
        for ($i = 0; $i < $read; $i++) {
            $reader->readNext();
        }

        $reader->moveBackward($move);
        $this->assertEquals($read - $move, $reader->getPosition());
    }

    public function testMoveForward()
    {
        $reader = new ArgsReader([1, 2, 3, 4, 5]);

        $expect = 4;
        $reader->moveForward($expect);
        $this->assertEquals($expect, $reader->getPosition());
    }

    public function testMoveForwardTriggerOutRange()
    {
        $this->expectException(\InvalidArgumentException::class);
        $reader = new ArgsReader([1, 2, 3, 4, 5]);
        $reader->moveForward(1000);
    }

    public function testMoveBackwardTriggerOutRange()
    {

        $this->expectException(\InvalidArgumentException::class);
        $reader = new ArgsReader([1, 2, 3, 4, 5]);
        $reader->moveForward(1000);
    }

    public function test__construct()
    {
        $reader = new ArgsReader([0, 1, 2, 3]);

        $this->assertInstanceOf(ArgsReader::class, $reader);
    }

    public function testGetPosition()
    {
        $reader = new ArgsReader([0, 1, 2, 3, 4]);

        $this->assertEquals(0, $reader->getPosition());
        $reader->readNext();
        $this->assertEquals(1, $reader->getPosition());
    }
}
