<?php

namespace tests;

use CarGame\DirectionEnum;
use PHPUnit\Framework\TestCase;


class DirectionEnumTest extends TestCase
{
    public function testConstantsCount()
    {
        $this->assertEquals(4, DirectionEnum::count());
    }

    public function testConstants()
    {
        $this->assertEquals(0, DirectionEnum::B()->getValue());
        $this->assertEquals(1, DirectionEnum::W()->getValue());
        $this->assertEquals(2, DirectionEnum::N()->getValue());
        $this->assertEquals(3, DirectionEnum::E()->getValue());
    }
}
