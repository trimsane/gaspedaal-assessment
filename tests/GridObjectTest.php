<?php

namespace tests;

use CarGame\DirectionEnum;
use CarGame\GridObject;
use CarGame\Position;
use PHPUnit\Framework\TestCase;

class GridObjectMock extends GridObject {

    public function __construct(Position $position)
    {
        $this->position = $position;
    }
}

class GridObjectTest extends TestCase
{
    /** @var GridObjectMock */
    protected $stub;

    protected function setUp()
    {
        $this->stub = new GridObjectMock(new Position(1, 0, 'N'));
    }

    public function testGetX()
    {
        $this->assertEquals(1, $this->stub->getX());
    }

    public function testGetY()
    {
        $this->assertEquals(0, $this->stub->getY());
    }

    public function testGetDirection()
    {
        $this->assertEquals(DirectionEnum::N(), $this->stub->getDirection());
    }

    public function test__toString()
    {
        $this->assertEquals("1 0 N", (string)$this->stub);
    }
}
