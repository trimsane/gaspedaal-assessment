<?php

namespace tests;

use Utils\ArgsParser;
use PHPUnit\Framework\TestCase;
use Utils\ArgsReader;

class ArgsParserTest extends TestCase
{
    public function parserDataProvider()
    {
        return [
            [
                '20 20 0 0 N 10 19 N FFFFEEFF FFFEEEFF',
                [
                    'grid:int,int:once',
                    'carPositions:int,int,char:many',
                    'commands:string:many',
                ],
                [
                    'grid' => [20, 20],
                    'carPositions' => [
                        [0, 0, 'N'],
                        [10, 19, 'N']
                    ],
                    'commands' => [["FFFFEEFF"], ["FFFEEEFF"]]
                ]
            ]
        ];
    }

    /**
     * @dataProvider parserDataProvider
     * @param array $args
     * @param array $parsingSchema
     * @param array $expected
     */
    public function testParse(string $args, array $parsingSchema, array $expected)
    {
        $parser = new ArgsParser(explode(' ', $args), $parsingSchema);
        $actual = $parser->parse();

        $this->assertEquals($expected, $actual);
    }
}
