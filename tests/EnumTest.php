<?php
/**
 * Created by PhpStorm.
 * User: trimsane
 * Date: 7/17/2018
 * Time: 5:17 AM
 */

namespace src\Utils;

use Utils\Enum;
use PHPUnit\Framework\TestCase;

class TestEnum extends Enum {
    public const I = -1;
    public const LIKE = 2;
    public const CHEESE = 3;
}

class EnumTest extends TestCase
{

    public function testCount()
    {
        $this->assertEquals(3, TestEnum::count());
    }


    public function fromValueProvider() {
        return [
            [TestEnum::I(), TestEnum::I],
            [TestEnum::LIKE(), TestEnum::LIKE],
            [TestEnum::CHEESE(), TestEnum::CHEESE],
        ];
    }

    /**
     * @dataProvider fromValueProvider
     * @param TestEnum $expected
     * @param int $value
     */
    public function testGetFromValue(TestEnum $expected, int $value)
    {
        $this->assertEquals($expected, TestEnum::getFromValue($value));
    }

    public function test__toString()
    {
         $this->assertEquals((string)TestEnum::LIKE(), "LIKE");
    }

    public function fromNameProvider() {
        return [
            [TestEnum::I(), "I"],
            [TestEnum::LIKE(), "LIKE"],
            [TestEnum::CHEESE(), "CHEESE"],
        ];
    }

    /**
     * @dataProvider fromNameProvider
     * @param TestEnum $expected
     * @param string $name
     */
    public function testGetFromName(TestEnum $expected, string $name)
    {
        $this->assertEquals($expected, $name);
    }


    /**
     * @dataProvider fromValueProvider
     * @param TestEnum $enum
     * @param int $expected
     */
    public function testGetValue(TestEnum $enum, int $expected)
    {
        $this->assertEquals($expected, $enum->getValue());
    }
}
