<?php

namespace tests;

use CarGame\Game;
use PHPUnit\Framework\TestCase;

/**
 * Class GameTest
 * Contains integration tests
 * @package tests
 */
class GameTest extends TestCase
{
    public function usageDataProvider()
    {
        return [
            //success cases
            [
                '20 20 0 0 N FFFFFRFFRFLFF',
                self::generateSuccessResult(['4 4 E'])
            ],
            [
                '20 20 0 0 N 12 9 E FFFFFRFFRFLFF FFRFRFLFFLFFR',
                self::generateSuccessResult(['4 4 E', '15 6 B'])
            ],
            [
                '20 20 0 0 N 12 9 E 12 5 E FFFFFRFFRFLFF FFRFRFLFFLFFR FFFRFFFL',
                self::generateSuccessResult(['4 4 E', '15 6 B', '15 2 E'])
            ],

            //same positions
            [
                '20 20 0 0 N 0 0 N FFFFFRFFRFLFF FFRFRFLFFLFFR',
                self::generateSamePositions(
                    ['0 0 N'],
                    [1 => '0 0 N']
                )
            ],
            [
                '20 20 0 0 N 0 0 N 0 0 N FFFFFRFFRFLFF FFRFRFLFFLFFR FFRFRFLFFLFFR',
                self::generateSamePositions(
                    ['0 0 N'],
                    [1 => '0 0 N', 2 => '0 0 N']
                )
            ],

            //different input
            [
                '20 20 0 0 N 0 0 N 0 0 N FFFFFRFFRFLFF FFRFRFLFFLFFR',
                self::generateDiffParamCount(3, 2)
            ],

            //parsing messages
            [
                '20 20 0 0 S, FFFRF',
                self::generateParsingMessage('char', 'S,', 5, '(carPositions:int,int,char:many)')
            ],

            //invalid parameters
            [
                '-10 20 0 0 N FFFRF',
                'Error: sizeX is negative' . PHP_EOL
            ],
            [
                '20 20 0 0 S FFFRF',
                'Error: Name `S`` does not exist in CarGame\DirectionEnum' . PHP_EOL
            ],
            [
                '20 20 0 0 E FFFRE',
                'Error: Name `E`` does not exist in CarGame\MoveCommandEnum' . PHP_EOL
            ]
        ];
    }

    /**
     * @dataProvider usageDataProvider
     * @param string $params
     * @param string $expecting
     */
    public function testRun(string $params, string $expecting)
    {
        @$this->expectOutputString($expecting);

        Game::run(explode(' ', $params));
    }

    private static function generateSuccessResult(array $input): string
    {
        $result = "";
        foreach ($input as $index => $item) {
            $result .= "Car $index: $item" . PHP_EOL;
        }
        return $result;
    }

    private static function generateSamePositions(array $input, array $total): string
    {
        $result = 'Error: Several cars\'ve already used same positions: ' . PHP_EOL;
        foreach ($total as $index => $samePosition) {
            $result .= 'Owner: Car №: '. array_search($samePosition, $input) .', ';
            $result .= "Car №: {$index} position:" . $samePosition . PHP_EOL;
        }

        return $result;
    }

    private static function generateDiffParamCount(int $positions, int $commands): string {
        $result = 'Positions and commands number are different. ' . PHP_EOL;
        $result  .= "Total positions:$positions" . PHP_EOL;
        $result   .= "Total commands:$commands" . PHP_EOL;

        return $result;
    }

    private static function generateParsingMessage(string $expected, string $got, int $position, string $query): string {
        $msg = "Error: expected type: `$expected`, but got: `$got`, ";
        $msg .= "position: $position, in arg query: $query " .PHP_EOL;
        return $msg;
    }
}
