<?php
/**
 * Created by PhpStorm.
 * User: trimsane
 * Date: 7/16/2018
 * Time: 10:45 PM
 */

namespace tests;

use CarGame\Grid;
use CarGame\GridObject;
use CarGame\MoveCommandEnum;
use CarGame\Position;
use CarGame\SmartCar;
use PHPUnit\Framework\TestCase;

class SmartCarTest extends TestCase
{

    public function test__construct()
    {
        $car = new SmartCar(
            new Grid(20, 20),
            new Position(10, 10, 'N'),
            [
                MoveCommandEnum::F(),
                MoveCommandEnum::L(),
                MoveCommandEnum::R()
            ]
        );

        $this->assertInstanceOf(SmartCar::class, $car);
        $this->assertInstanceOf(GridObject::class, $car);
    }

    private static $grid;

    protected function setUp()
    {
        self::$grid = new Grid(20, 20);
    }

    public function moveDataProvider()
    {
        return [
            ['0 0 N', 'FFFFFRFFRFLFF', '4 4 E'],
            ['12 9 E', 'FFRFRFLFFLFFR', '15 6 B'],
            ['20 20 B', 'RFFLFFFFLFF', '20 16 E'],
            //Car crash
            ['0 0 E', 'RFFRFFFRFFR', '0 0 B'],
            ['0 0 N', 'FFFFFFFFFFFFFFFFFFFFF', '0 20 N']
        ];
    }

    /**
     *
     * @dataProvider moveDataProvider
     * @param string $startPos
     * @param string $moves
     * @param string $expectedTarget
     */
    public function testMove(string $startPos, string $moves, string $expectedTarget)
    {
        $enum_moves = array_map(function ($char) {
            return MoveCommandEnum::getFromName($char);
        }, str_split($moves));

        list($x, $y, $d) = explode(' ', $startPos);
        $car = new SmartCar(self::$grid, new Position($x, $y, $d), $enum_moves);
        $car->move();

        $this->assertEquals((string)$car, $expectedTarget);
    }

//    /**
//     *
//     * @dataProvider moveDataProvider
//     * @param string $startPos
//     * @param string $moves
//     * @param string $expectedTarget
//     */
//    public function testMovePersistent(string $startPos, string $moves, string $expectedTarget)
//    {
//        $enum_moves = array_map(function ($char) {
//            return MoveCommandEnum::getFromName($char);
//        }, str_split($moves));
//
//        list($x, $y, $d) = explode(' ', $startPos);
//        $car = new SmartCar(self::$grid, new Position($x, $y, $d), $enum_moves);
//
//        foreach ($car->movePersistent() as $position) {
//            echo "[$position]";
//        }
//
//        $this->assertEquals((string)$car, $expectedTarget);
//    }
}
