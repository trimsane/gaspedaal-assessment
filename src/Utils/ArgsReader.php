<?php

namespace Utils;

/**
 * Class ArgsReader
 * Reads data from a parameter array.
 *
 * ATTENTION: this class like others in @package Utils was implemented just for fun!
 * THE MAIN GOAL of this is learning, practice and deeper understanding of PHP.
 * I prefer ready-made and well-tested solutions for production.
 *
 * @package Utils
*/
class ArgsReader
{
    /** @var int */
    private $position;
    /** @var int */
    private $length;
    /** @var mixed[] */
    private $args;


    /**
     * ArgsReader constructor.
     * @param array $args
     */
    public function __construct(array $args)
    {
        $this->position = 0;

        $this->args = $args;
        $this->length = count($args);
    }

    /**
     * Shifts position to the right
     * @param int $shift
     */
    public function moveForward(int $shift)
    {
        $this->move($shift);
    }

    /**
     * Shifts position to the right
     * @param int $shift
     */
    public function moveBackward(int $shift)
    {
       $this->move(-$shift);
    }

    private function move(int $shift) {
        $newPosition = $this->position + $shift;

        if ($newPosition < 0 || $newPosition > $this->length) {
            throw new \InvalidArgumentException("New position: `$newPosition` out of range");
        }

        $this->position = $newPosition;
    }

    /**
     * Reads next items from array, returns null if out of range.
     *
     * @return mixed|null
     */
    public function readNext()
    {
        if (!$this->canRead())
            return null;

        return $this->args[$this->position++];
    }

    private function canRead()
    {
        return $this->position < $this->length;
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }
}