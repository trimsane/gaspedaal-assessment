<?php

namespace Utils;

/**
 * Class Enum.
 * Base class which allows to emulate Enumeration classes.
 * Uses cache of instances for reducing memory allocation.
 *
 * Represents functionality of @see \SplEnum.
 *
 * ATTENTION: this class like others in @package Utils was implemented just for fun!
 * THE MAIN GOAL of this is learning, practice and deeper understanding of PHP.
 * I prefer ready-made and well-tested solutions for production.
 *
 * @package Utils
 */
abstract class Enum
{
    /** @var CachedInstance[\ReflectionClass] */
    private static $cache = [];

    /** @var string */
    private $name;
    /** @var bool|string|int */
    private $value;

    /**
     * Enum constructor.
     * @param string $key
     * @param $value (bool|string|int)
     */
    final private function __construct(string $key, $value)
    {
        $this->name = $key;
        $this->value = $value;
    }

    /**
     * Creates enum instance with short static constructor
     *
     * @param string $name
     * @param array $arguments
     *
     * @return static
     *
     * @throws \InvalidArgumentException
     */
    final public static function __callStatic(string $name, array $arguments)
    {
        return static::getFromName($name);
    }

    /**
     * Returns count of enum's elements
     *
     * @return int
     */
    final public static function count(): int
    {
        return count(self::getCachedInstance()->constants);
    }

    /**
     * Gets enum instance using name.
     *
     * @param string $name
     * @return mixed
     *
     * @throws \InvalidArgumentException
     */
    final public static function getFromName(string $name)
    {
        $cached = static::getCachedInstance();
        $constants = $cached->constants;

        if (!array_key_exists($name, $constants)) {
            throw new \InvalidArgumentException(
                "Name `${name}`` does not exist in " . static::class);
        }

        $value = $constants[$name];
        $instances = $cached->instances;
        if (!array_key_exists($name, $instances)) {
            $instances[$name] = new static($name, $value);
        }

        return $instances[$name];
    }

    /**
     * Gets enum instance from value,
     *
     * @param $value
     * @return mixed
     */
    final public static function getFromValue($value)
    {
        $cached = static::getCachedInstance();
        $cachedConstants = $cached->constants;

        $key = array_search($value, $cachedConstants);
        if (!$key) {
            throw new \InvalidArgumentException(
                "Value `${value}``does not exist in " . static::class);
        }

        $cachedInstances = $cached->instances;
        $instance = array_search($value, $cachedInstances);
        if (!$instance) {
            $cachedInstances[$key] = new static($key, $value);
        }

        return $cachedInstances[$key];
    }

    final private static function getCachedInstance(): CachedInstance
    {
        $class = static::class;
        if (!array_key_exists($class, self::$cache)) {
            $reflection = new \ReflectionClass($class);

            $cached = new CachedInstance();
            $cached ->constants = $reflection->getConstants();

            self::$cache[$class] = $cached;
            return $cached;
        }

        return self::$cache[$class];
    }

    final public function __toString()
    {
        return (string)$this->name;
    }

    final public function getValue()
    {
        return $this->value;
    }
}

/**
 * Class CacheInstance
 *
 * Container which stores cached enum's instances and reflected constants.
 *
 * @package Utils
 */
final class CachedInstance
{
    /** @var \ReflectionClass[] */
    public $instances = [];
    /** @var \ReflectionClassConstant[] */
    public $constants = [];
}