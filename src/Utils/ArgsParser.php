<?php

namespace Utils;

/**
 * Class ArgsParser
 * Parses sequence of CLI parameters, uses array of string queries, represents `getopt` functionality.
 *
 * Supported types: int, string, char;
 * Supported Matches, once, many(at least once).
 *
 * ATTENTION: this class like others in @package Utils was implemented just for fun!
 * THE MAIN GOAL of this is learning, practice and deeper understanding of PHP.
 * I prefer ready-made and well-tested solutions for production.
 *
 * @package Utils
 */
final class ArgsParser
{
    private $reader;
    private $argQueries;

    /**
     * ArgsParser constructor.
     * @param array $args
     * @param array $parsingSchema
     */
    public function __construct(array $args, array $parsingSchema)
    {
        $this->reader = new ArgsReader($args);
        $this->argQueries = $parsingSchema;
    }

    /**
     * Parses parameters, throws ParseError if it fails.
     * @return array
     * @throws \ParseError
     */
    public function parse(): array
    {
        $result = [];
        foreach ($this->argQueries as $argQuery) {
            $query = ArgQuery::parse($argQuery);

            $result[$query->getArgName()] = $this->parseByType($query);
        }

        return $result;
    }

    private function parseByType(ArgQuery $query): array
    {
        switch ($query->getParsingType()) {
            case ArgumentMatchEnum::ONCE:
                return $this->parseOnce($query);
            case ArgumentMatchEnum::MANY:
                return $this->atLeastOnce($query);
        }
    }

    private function parseOnce(ArgQuery $query, bool $throwError = true)
    {
        $params = [];
        foreach ($query->getTypes() as $i => $type) {
            $parameter = $this->reader->readNext();
            if (!self::isValidParameter($type, $parameter)) {
                if ($throwError)
                    throw self::formatError($query, $type, $parameter, $this->reader->getPosition());

                $this->reader->moveBackward($i + 1);
                return false;
            }

            $params[] = $parameter;
        }

        return $params;
    }

    private function atLeastOnce(ArgQuery $query): array
    {
        $result = [];
        $result[] = $this->parseOnce($query);

        while ($data = $this->parseOnce($query, false)) {
            $result[] = $data;
        }
        return $result;
    }

    private static function isValidParameter($type, $parameter)
    {
        switch ($type) {
            case ArgumentTypeEnum::INT:
                return is_numeric($parameter);
                break;
            case ArgumentTypeEnum::STRING:
                return is_string($parameter);
                break;
            case ArgumentTypeEnum::CHAR:
                return self::isChar($parameter);
                break;
            default:
                throw new \InvalidArgumentException($type);
        }
    }

    private static function isChar($str): bool
    {
        return is_string($str) && strlen($str) === 1;
    }

    private static function formatError(ArgQuery $query, string $type, $parameter, int $readerPos) {
        $msg = "expected type: `$type`, but got: `$parameter`, ";
        $msg .= "position: $readerPos, in arg query: ({$query->getSource()}) ";
        return  new ParseException($msg);
    }
}

/**
 * Class ArgumentMatchEnum
 * @package Utils
 */
class ArgumentMatchEnum extends Enum {
    public const ONCE = 'once';
    public const MANY = 'many';
}

/**
 * Class ArgumentTypeEnum
 * @package Utils
 */
class ArgumentTypeEnum extends Enum {
    public const INT = 'int';
    public const STRING = 'string';
    public const CHAR = 'char';
}

/**
 * Class ArgQuery
 * Stores and Parses query string for ArgParser class
 * @package Utils
 */
final class ArgQuery {
    private $types;
    private $argMatch;
    private $argName;

    private $source;

    private function __construct(string $source)
    {
        $this->source = $source;
    }

    /**
     * Parses arg query using grammar (argname:type...,:)
     * @param string $argQuery
     * @return ArgQuery
     */
    public static function parse(string $argQuery) : self {
        list($argName, $argsTypes, $parseType) = explode(':', $argQuery);

        $instance = new self($argQuery);

        $instance->argName = $argName;
        $instance->argMatch = $parseType;
        $instance->types = explode(',', $argsTypes);

        return $instance;
    }

    /**
     * @return array
     */
    public function getTypes(): array
    {
        return $this->types;
    }

    /**
     * @return mixed
     */
    public function getParsingType()
    {
        return $this->argMatch;
    }

    /**
     * @return mixed
     */
    public function getArgName()
    {
        return $this->argName;
    }

    /**
     * @return string
     */
    public function getSource(): string
    {
        return $this->source;
    }
}

class ParseException extends \Exception {
}