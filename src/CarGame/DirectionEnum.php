<?php

namespace CarGame;


use Utils\Enum;

/**
 * Class DirectionEnum
 * Specifies enumeration of directions.
 * @package CarGame
 */
class DirectionEnum extends Enum
{
    public const B = 0;
    public const W = 1;
    public const N = 2;
    public const E = 3;
}