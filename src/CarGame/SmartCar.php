<?php

namespace CarGame;

class SmartCar extends GridObject
{
    /** @var Grid */
    private $grid;
    /** @var array */
    private $commands;

    /**
     * Car constructor.
     * @param Grid $grid
     * @param Position $position
     * @param array $commands
     */
    function __construct(Grid $grid, Position $position, array $commands)
    {
        $this->grid = $grid;
        $this->position = $position;
        $this->commands = $commands;
    }

    /**
     * Moves car to the destination.
     */
    public function move()
    {
        foreach ($this->movePersistent() as $position) {}
    }

    /**
     * Moves car to the destination yielding path.
     */
    public function movePersistent()
    {
        $isOutOfGrid = false;
        foreach ($this->commands as $command) {

            //don't wanna implement strategy pattern for 3 cases:
            switch ($command->getValue()) {
                case MoveCommandEnum::F:
                    $nextPosition = $this->calcNextPosition();
                    $isOutOfGrid = $this->checkCollisions($nextPosition);
                    if (!$isOutOfGrid)
                        $this->position = $nextPosition;

                    break;
                case MoveCommandEnum::L:
                    $this->turnLeft();
                    break;
                case MoveCommandEnum::R:
                    $this->turnRight();
                    break;
                default:
                    throw new \InvalidArgumentException("invalid command ${command}");
            }

            yield (string)$this->position;

            if ($isOutOfGrid)
                break;
        }
    }

    private function checkCollisions($nextPosition)
    {
        $gridX = $this->grid->getSizeX();
        $gridY = $this->grid->getSizeY();

        return
            $nextPosition->x < 0 || $nextPosition->y < 0 ||
            $nextPosition->x > $gridX || $nextPosition->y > $gridY;
    }

    private function calcNextPosition(): Position
    {
        $position = clone $this->position;
        $direction = $position->direction;

        switch ($direction->getValue()) {
            case DirectionEnum::N:
                $position->y += 1;
                break;
            case DirectionEnum::B:
                $position->y -= 1;
                break;
            case DirectionEnum::E:
                $position->x += 1;
                break;
            case DirectionEnum::W:
                $position->x -= 1;
                break;
            default:
                throw new \InvalidArgumentException($direction);
        }

        return $position;
    }

    private function turnLeft()
    {
        $this->position->direction = $this->turn(-1);
    }

    private function turnRight()
    {
        $this->position->direction = $this->turn(1);
    }

    private function turn(int $shift): DirectionEnum
    {
        $directionIndex = $this->position->direction->getValue();

        //due to the fact that direction enum is an array of sides ['B', 'W', 'S', 'E']
        //so -1 moves left and +1 moves right
        //this expression returns indexes between -1 and 4, this trick allows to skip unreadable if-else
        $newDirection = ($directionIndex + $shift) % DirectionEnum::count();
        return $newDirection >= 0
            ? DirectionEnum::getFromValue($newDirection)
            : DirectionEnum::E();
    }
}