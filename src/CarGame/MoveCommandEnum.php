<?php

namespace CarGame;

use Utils\Enum;

/**
 * Class MoveCommandEnum
 * Specifies enumeration of move commands.
 * @package CarGame
 */
class MoveCommandEnum extends Enum
{
    public const F = 0;
    public const L = -1;
    public const R = 1;
}