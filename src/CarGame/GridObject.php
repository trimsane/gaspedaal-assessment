<?php
/**
 * Created by PhpStorm.
 * User: trimsane
 * Date: 7/16/2018
 * Time: 9:35 PM
 */

namespace CarGame;

/**
 * Class GridObject
 * Basic class for all GridObjects
 *
 * @package CarGame
 */
abstract class GridObject
{
    /** @var Position */
    protected $position;

    /**
     * @return int
     */
    public function getX(): int
    {
        return $this->position->x;
    }

    /**
     * @return int
     */
    public function getY(): int
    {
        return $this->position->y;
    }

    /**
     * @return DirectionEnum
     */
    public function getDirection(): DirectionEnum
    {
        return $this->position->direction;
    }


    /**
     * @return string
     */
    public function __toString()
    {
        return "{$this->getX()} {$this->gety()} {$this->getDirection()}";
    }

}