<?php

namespace CarGame;

/**
 * Class Position
 * Contains position info for the grid items;
 * @package CarGame
 */
class Position
{
    /** @var int  */
    public $x;
    /** @var int  */
    public $y;
    /** @var DirectionEnum  */
    public $direction;

    public function __construct(int $x, int $y, string $direction)
    {
        $this->x = $x;
        $this->y = $y;
        $this->direction = DirectionEnum::getFromName($direction);
    }

    public function __toString()
    {
        return "$this->x $this->y $this->direction";
    }
}