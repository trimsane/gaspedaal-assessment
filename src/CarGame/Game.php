<?php


namespace CarGame;

use Utils\ArgsParser;

class Game
{
    /**
     * @param array $args
     */
    public static function run(array $args)
    {
        try {
            $input = self::parseArgs($args);
            $positionParams = $input['carPositions'];
            $commands = $input['commands'];

            if (self::hasSameParametersCount($positionParams, $commands)) {
                echo 'Positions and commands number are different. ' . PHP_EOL;
                echo 'Total positions:' . count($positionParams) . PHP_EOL;
                echo 'Total commands:' . count($commands) . PHP_EOL;
                return;
            }

            $carPositions = self::parsePositions($positionParams);
            if ($samePositions = self::getDuplicates($carPositions)) {
                echo 'Error: Several cars\'ve already used same positions: ' . PHP_EOL;

                foreach ($samePositions as $index => $samePosition) {
                    echo 'Owner: Car №: ' . array_search($samePosition, $carPositions) . ', ';
                    echo "Car №: {$index} position:" . $samePosition . PHP_EOL;
                }
                return;
            }

            list($gridX, $gridY) = $input['grid'];
            $grid = new Grid($gridX, $gridY);
            $cars = [];
            for ($i = 0; $i < count($commands); $i++) {
                $cars[] = self::createCar($grid, $carPositions[$i], $commands[$i][0]);
            }

            foreach ($cars as $index => $car) {
                $car->move();
                echo "Car $index: " . $car . PHP_EOL;
            }
        } catch (\Exception $e) {
            echo 'Error: ' . $e->getMessage() . PHP_EOL;
        }
    }

    /**
     * @param $positionParams
     * @param $commands
     * @return bool
     */
    private static function hasSameParametersCount($positionParams, $commands): bool
    {
        return count($positionParams) != count($commands);
    }

    /**
     * @param $carPosition
     * @return array
     */
    private static function parsePositions(array $carPosition): array
    {
        return array_map(function ($position) {
            list($x, $y, $direction) = $position;
            return new Position($x, $y, DirectionEnum::getFromName($direction));
        }, $carPosition);
    }

    /**
     * @param $grid
     * @param $position
     * @param $commandStr
     * @return SmartCar
     */
    private static function createCar($grid, $position, $commandStr): SmartCar
    {
        $parsedCommands = array_map(function ($commandChar) {
            return MoveCommandEnum::getFromName($commandChar);
        }, str_split($commandStr));

        return new SmartCar($grid, $position, $parsedCommands);
    }

    private static $parsingSchema = [
        'grid:int,int:once',
        'carPositions:int,int,char:many',
        'commands:string:many',
    ];

    /**
     * @param $args
     * @return array
     */
    private static function parseArgs($args): array
    {
        $parser = new ArgsParser($args, self::$parsingSchema);

        return $parser->parse();
    }

    /**
     * @param $array
     * @return array
     */
    private static function getDuplicates(array $array): array
    {
        return array_diff_key($array, array_unique($array));
    }
}
