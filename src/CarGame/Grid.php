<?php

namespace CarGame;
use Prophecy\Exception\InvalidArgumentException;

/**
 * Class Grid
 * Represents rectangular grid
 * @package CarGame
 */
class Grid
{
    /** @var int */
    private $sizeX;
    /** @var int */
    private $sizeY;

    /**
     * Grid constructor.
     * @param int $sizeX
     * @param int $sizeY
     */
    public function __construct(int $sizeX, int $sizeY)
    {
        if ($sizeX < 0)
            throw new InvalidArgumentException('sizeX is negative');
        if ($sizeY < 0)
            throw new InvalidArgumentException('sizeY is negative');

        $this->sizeX = $sizeX;
        $this->sizeY = $sizeY;
    }

    /**
     * @return int
     */
    public function getSizeX(): int
    {
        return $this->sizeX;
    }

    /**
     * @return int
     */
    public function getSizeY(): int
    {
        return $this->sizeY;
    }
}