<?php

use CarGame\Game;

require_once '../vendor/autoload.php';

// skip first script path parameter
array_shift($argv);

Game::run($argv);
