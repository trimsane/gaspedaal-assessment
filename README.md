#Gaspedaal Assessment

##Description of the problem

Gaspedaal is moving into self-driving cars, and we started working on a software that takes care of the car navigation system in a 2D map.
Your task is to write a program that puts two cars in a rectangular 2D grid, receives movement instructions and returns the final location of these
two cars.
The rectangular map is defined by two integer values [x y] defining its north-east corner. The south-west corner is defined by [0 0].
The starting position of each car is defined by its [x y] position and a letter indicating the direction it’s facing; N for north, W for west, S for south
and B for east
The cars accept three kind of movement instructions: L, R and F:
L makes the car turn to the left.
R makes the car turn to the right.
F makes the car move one position forward.
Note that the turning commands do not move the car from its position, only turns it.
The movement instructions is list of any combination of these letters.

###Inputs
The program receives 5 inputs (arguments):
* [x y] - Integer values. North-east corner position of the rectangular map
* [x y z] - x and y are integers, z is a character. Starting position of the first car
* [x y z] - x and y are integers, z is a character. Starting position of the second car
* [string] - string composed by any combination of letters L, R and F defining the instructions to the first car
* [string] - string composed by any combination of letters L, R and F defining the instructions to the second car

##Assumptions:
*Assume that the second car does not start moving until the first one finishes.
*Assume that the second car starting position will never be the same as the first one.

##Summary
All classes in Utils namespace were implemented just for fun!
Main goal of this is learning, practice and deeper understanding of PHP.
I prefer ready-made and well tested solutions for production.
 
##Additional assumptions:
* Grid coordinates can't be negative;
* Car executes all commands;
* If car is moving forward and can hit the border it stops. 

##How to run:
```shell
To run the assessment : php assessment.php 20 20 0 0 N 12 9 E FFFFFRFFRFLFF FFRFRFLFFLFFR
```


